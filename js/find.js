// console.log(location.search);
let singleData = document.querySelector("#singleData");
let queryParameters = location.search;

let sanitizedQP = queryParameters.substring(1); // Remove question mark(?) from url search

console.log(sanitizedQP);

let sanitizedQPArray = sanitizedQP.split("&");

console.log(sanitizedQPArray);

params = {};

sanitizedQPArray.forEach(function(sanitizedQP) {
  paramsKeyValueArray = sanitizedQP.split("=");
  params[paramsKeyValueArray[0]] = paramsKeyValueArray[1];
});

console.log(params);

renderSingleJob(params.id);

function renderSingleJob(id) {
  const targetJob = findById(id);
  console.log(targetJob);

  if (id) {
    let list = "";
    list += ` <div class="single-post d-flex flex-row">
                <div class="details">
                  <div class="title d-flex flex-row justify-content-between">
                    <div class="titles">
                      <a href="singleJob.html">
                        <h4>${targetJob.jobTitle}</h4>
                      </a>
                      <h6>${targetJob.companyName}</h6>
                    </div>
                  </div>
                  <p class="description">
                    ${targetJob.jobDescription}
                  </p>
                  <h5>Job Nature: Full time</h5>
                  <p class="address"> ${targetJob.location}</p>
                  <p class="salary">${targetJob.salary}</p>
                  <p class="published-on mr-5 d-inline">Published on: ${targetJob.publishedOn}</p>
                  <p class="application-deadline d-inline">Application Deadline: ${targetJob.applicationDeadline}</p>
                </div>
                </div>
                <div class="single-post d-flex flex-row">
                  <div class="details">
                    <div class="title d-flex flex-row justify-content-between">
                      <div class="titles">
                        <h4>Job Description</h4>
                      </div>
                    </div>
                    <p class="description">
                      Lorem, ipsum dolor sit amet consectetur adipisicing elit. Suscipit ea officia, obcaecati odio, minus
                      maxime incidunt atque, nihil esse expedita repellat dolores voluptatem! Modi nihil rerum, maiores
                      consectetur perspiciatis debitis. Doloremque id non provident odit impedit vero eveniet nemo quos fugiat
                      libero, et el, blanditiis, earum ab nemo ducimus modi doloremque aliquam nam, per, beatae nihil, dicta
                      velit magni repudiandae eum nostrum ducimus! Assumenda, quas! Architecto recusandae velit sed
                      consequatur voluptates. Eaque impedit voluptas deleniti blanditiis possimus earum adipisci,
                      necessitatibus commodi facilis quidem aliquid numquam suscipit voluptatibus quia.
                    </p>
                    <p class="description">
                      Lorem, ipsum dolor sit amet consectetur adipisicing elit. Suscipit ea officia, obcaecati odio, minus
                      maxime incidunt atque, nihil esse expedita repellat dolores voluptatem! Modi nihil rerum, maiores
                      consectetur perspiciatis deemo ducimus modi doloremque aliquam nam, per, beatae nihil, dicta velit magni
                      repudiandae eum nostrum ducimus! Assumenda, quas! Architecto recusandae velit sed consequatur
                      voluptates. Eaque impedit voluptas deleniti blanditiis possimus earum adipisci, necessitatibus commodi
                      facilis quidem aliquid numquam suscipit voluptatibus quia.
                    </p>
                  </div>
                </div>
                <div class="single-post d-flex flex-row">
                  <div class="details">
                    <div class="title d-flex flex-row justify-content-between">
                      <div class="titles">
                        <h4>Responsiblilties</h4>
                      </div>
                    </div>
                    <p class="description">
                      Lorem, ipsum dolor sit amet consectetur adipisicing elit. Suscipit ea officia, obcaecati odio, minus
                      maxime incidunt atque, nihil esse expedita repellat dolores voluptatem! Modi nihil rerum, maiores
                      consectetur perspiciatis debitis. Doloremque id non provident odit impedit vero eveniet nemo quos fugiat
                      libero, et el, blanditiis, earum ab nemo ducimus modi doloremque aliquam nam, per, beatae nihil, dicta
                      velit magni repudiandae eum nostrum ducimus! Assumenda, quas! Architecto recusandae velit sed
                      consequatur voluptates. Eaque impedit voluptas deleniti blanditiis possimus earum adipisci,
                      necessitatibus commodi facilis quidem aliquid numquam suscipit voluptatibus quia.
                    </p>
                    <p class="description">
                      Lorem, ipsum dolor sit amet consectetur adipisicing elit. Suscipit ea officia, obcaecati odio, minus
                      maxime incidunt atque, nihil esse expedita repellat dolores voluptatem! Modi nihil rerum, maiores
                      consectetur perspiciatis deemo ducimus modi doloremque aliquam nam, per, beatae nihil, dicta velit magni
                      repudiandae eum nostrum ducimus! Assumenda, quas! Architecto recusandae velit sed consequatur
                      voluptates. Eaque impedit voluptas deleniti blanditiis possimus earum adipisci, necessitatibus commodi
                      facilis quidem aliquid numquam suscipit voluptatibus quia.
                    </p>
                  </div>
                </div>
                <div class="single-post d-flex flex-row">
                  <div class="details">
                    <div class="title d-flex flex-row justify-content-between">
                      <div class="titles">
                        <h4>Requirements</h4>
                      </div>
                    </div>
                    <p class="description">
                      Lorem, ipsum dolor sit amet consectetur adipisicing elit. Suscipit ea officia, obcaecati odio, minus
                      maxime incidunt atque, nihil esse expedita repellat dolores voluptatem! Modi nihil rerum, maiores
                      consectetur perspiciatis debitis. Doloremque id non provident odit impedit vero eveniet nemo quos fugiat
                      libero, et el, blanditiis, earum ab nemo ducimus modi doloremque aliquam nam, per, beatae nihil, dicta
                      velit magni repudiandae eum nostrum ducimus! Assumenda, quas! Architecto recusandae velit sed
                      consequatur voluptates. Eaque impedit voluptas deleniti blanditiis possimus earum adipisci,
                      necessitatibus commodi facilis quidem aliquid numquam suscipit voluptatibus quia.
                    </p>
                    <p class="description">
                      Lorem, ipsum dolor sit amet consectetur adipisicing elit. Suscipit ea officia, obcaecati odio, minus
                      maxime incidunt atque, nihil esse expedita repellat dolores voluptatem! Modi nihil rerum, maiores
                      consectetur perspiciatis deemo ducimus modi doloremque aliquam nam, per, beatae nihil, dicta velit magni
                      repudiandae eum nostrum ducimus! Assumenda, quas! Architecto recusandae velit sed consequatur
                      voluptates. Eaque impedit voluptas deleniti blanditiis possimus earum adipisci, necessitatibus commodi
                      facilis quidem aliquid numquam suscipit voluptatibus quia.
                    </p>
                  </div>
                </div>`;

    singleData.innerHTML = list;
  } else {
    singleData.innerHTML = `<div class="single-post d-flex flex-row"> <div class="titles">
            <h4>No Jobs Found</h4>
        </div></div>`;
  }
}

function getFromLocalStorage() {
  let dataStore = localStorage.getItem("Details");
  if (dataStore) {
    // check if data exists
    return JSON.parse(dataStore);
  } else {
    return 0;
  }
}
