//Global controller

//Blog Controller

//UI Controller

const display = document.querySelector("#data");
const display2 = document.querySelector("#singlePost");
const display3 = document.querySelector("#commentSection");

function getBlogs() {
  fetch("https://jsonplaceholder.typicode.com/posts")
    .then(response => response.json())
    .then(json => {
      console.log(json);
      (function() {
        document.getElementById("first").addEventListener("click", firstPage);
        document.getElementById("next").addEventListener("click", nextPage);
        document
          .getElementById("previous")
          .addEventListener("click", previousPage);
        document.getElementById("last").addEventListener("click", lastPage);

        let list = json;
        let pageList = [];
        let currentPage = 1;
        let numberPerPage = 10;
        let numberOfPages = 0;

        function makeList() {
          numberOfPages = getNumberOfPages();
        }

        function getNumberOfPages() {
          return Math.ceil(list.length / numberPerPage);
        }

        function nextPage() {
          currentPage += 1;
          loadList();
          document.documentElement.scrollTop = 0;
        }

        function previousPage() {
          currentPage -= 1;
          loadList();
          document.documentElement.scrollTop = 0;
        }

        function firstPage() {
          currentPage = 1;
          loadList();
          document.documentElement.scrollTop = 0;
        }

        function lastPage() {
          currentPage = numberOfPages;
          loadList();
          document.documentElement.scrollTop = 0;
        }

        function loadList() {
          var begin = (currentPage - 1) * numberPerPage;
          var end = begin + numberPerPage;

          pageList = list.slice(begin, end);
          console.log(pageList);
          drawList();
          check();
        }

        function drawList() {
          display.innerHTML = "";
          pageList.forEach(el => {
            display.innerHTML += ` <div class="single-post"><div class="title" id="main-title"><div class="blog-content"><h4>${el.title}</h4><p>${el.body}</p><p>by<span class="lorem">Lorem ipsum</span></p><p>October 9, 2019</p></p><a href="view.html?id=${el.id}"class="view">View Post</a></div><div id="image">	<img src="images/adult-businessman-close-up-374820.jpg" id="indexImage" alt=""></div></div></div>`;
          });
        }
        function check() {
          document.getElementById("next").disabled =
            currentPage == numberOfPages ? true : false;
          document.getElementById("previous").disabled =
            currentPage == 1 ? true : false;
          document.getElementById("first").disabled =
            currentPage == 1 ? true : false;
          document.getElementById("last").disabled =
            currentPage == numberOfPages ? true : false;
        }

        function load() {
          makeList();
          loadList();
        }

        window.onload = load();
      })();
    });
}

getBlogs();

// Parse url parameter to get the
let queryParameters = location.search;

let sanitizedQP = queryParameters.substring(1); // Remove question mark(?) from url search

// console.log(sanitizedQP);

let sanitizedQPArray = sanitizedQP.split("&");

// console.log(sanitizedQPArray);

params = {};

sanitizedQPArray.forEach(function(sanitizedQP) {
  paramsKeyValueArray = sanitizedQP.split("=");
  params[paramsKeyValueArray[0]] = paramsKeyValueArray[1];
});

console.log(params);
renderSinglePost(params.id);

function renderSinglePost(id) {
  // getting a single blog post
  fetch(`https://jsonplaceholder.typicode.com/posts/${id}`)
    .then(response => response.json())
    .then(json => {
      console.log(json);

      display2.innerHTML = `<div class= "singlePost">
      <img src="images/Capital_One_57_photo-credit_-Connie-Zhou-940x650.jpg" alt="">
      <h3>${json.title}</h3>
      <p>${json.body}Lorem ipsum dolor sit amet Lorem ipsum, dolor sit amet consectetur adipisicing elit. Ipsa natus tenetur
        amet laudantium harum cupiditate, enim aut optio ex est repellat excepturi magni eius cum sint nobis totam
        deserunt veniam. consectetur adipisicing elit. Quisquam a in mollitia expedita beatae eligendi ullam,
        deleniti iste nisi maiores ut, sed saepe magnam quos quaerat, labore nemo odio. Voluptates.</p><h6>Lorem Ipsum</h6><p>Lorem ipsum dolor sit amet Lorem ipsum, dolor sit amet consectetur adipisicing elit. Ipsa natus tenetur
        amet laudantium harum cupiditate, enim aut optio ex est repellat excepturi magni eius cum sint nobis totam
        deserunt veniam. consectetur adipisicing elit. Quisquam a in mollitia expedita beatae eligendi ullam,
        deleniti iste nisi maiores ut, sed saepe magnam quos quaerat, labore nemo odio. Lorem ipsum dolor sit amet Lorem ipsum, dolor sit amet consectetur adipisicing elit. Ipsa natus teneturLorem ipsum dolor sit amet Lorem ipsum, dolor sit amet consectetur adipisicing elit. Ipsa natus tenetur
        amet laudantium harum cupiditate, enim aut optio ex est repellat excepturi magni eius cum sint nobis totam
        deserunt veniam. consectetur adipisicing elit. Quisquam a in mollitia expedita beatae eligendi ullam,
        deleniti iste nisi maiores ut, sed saepe magnam quos quaerat, labore nemo odio. Voluptates
        amet laudantium harum cupiditate, enim aut optio ex est repellat excepturi magni eius cum sint nobis totam
        deserunt veniam. consectetur adipisicing elit. Quisquam a in mollitia expedita beatae eligendi ullam,
        deleniti iste nisi maiores ut, sed saepe magnam quos quaerat, labore nemo odio. Voluptates
        deleniti iste nisi maiores ut, sed saepe magnam quos quaerat, labore nemo odio. Voluptates.</p><h6>Lorem Ipsum</h6><p>Lorem ipsum dolor sit amet Lorem ipsum, dolor sit amet consectetur adipisicing elit. Ipsa natus tenetur
        amet laudantium harum cupiditate, enim aut optio ex est repellat excepturi magni eius cum sint nobis totam
        deserunt veniam. consectetur adipisicing elit. Quisquam a in mollitia expedita beatae eligendi ullam,
        deleniti iste nisi maiores ut, sed saepe magnam quos quaerat, labore nemo odio. Lorem ipsum dolor sit amet Lorem ipsum, dolor sit amet consectetur adipisicing elit. Ipsa natus teneturLorem ipsum dolor sit amet Lorem ipsum, dolor sit amet consectetur adipisicing elit. Ipsa natus tenetur
        amet laudantium harum cupiditate, enim aut optio ex est repellat excepturi magni eius cum sint nobis totam
        deserunt veniam. consectetur adipisicing elit. Quisquam a in mollitia expedita beatae eligendi ullam,
        deleniti iste nisi maiores ut, sed saepe magnam quos quaerat, labore nemo odio. Voluptates
        amet laudantium harum cupiditate, enim aut optio ex est repellat excepturi magni eius cum sint nobis totam
        deserunt veniam. consectetur adipisicing elit. Quisquam a in mollitia expedita beatae eligendi ullam,
        deleniti iste nisi maiores ut, sed saepe magnam quos quaerat, labore nemo odio. Voluptates
        deleniti iste nisi maiores ut, sed saepe magnam quos quaerat, labore nemo odio. Voluptates.</p><h6>Lorem Ipsum</h6><p>Lorem ipsum dolor sit amet Lorem ipsum, dolor sit amet consectetur adipisicing elit. Ipsa natus tenetur
        amet laudantium harum cupiditate, enim aut optio ex est repellat excepturi magni eius cum sint nobis totam
        deserunt veniam. consectetur adipisicing elit. Quisquam a in mollitia expedita beatae eligendi ullam,
        deleniti iste nisi maiores ut, sed saepe magnam quos quaerat, labore nemo odio. Lorem ipsum dolor sit amet Lorem ipsum, dolor sit amet consectetur adipisicing elit. Ipsa natus teneturLorem ipsum dolor sit amet Lorem ipsum, dolor sit amet consectetur adipisicing elit. Ipsa natus tenetur
        amet laudantium harum cupiditate, enim aut optio ex est repellat excepturi magni eius cum sint nobis totam
        deserunt veniam. consectetur adipisicing elit. Quisquam a in mollitia expedita beatae eligendi ullam,
        deleniti iste nisi maiores ut, sed saepe magnam quos quaerat, labore nemo odio. Voluptates
        amet laudantium harum cupiditate, enim aut optio ex est repellat excepturi magni eius cum sint nobis totam
        deserunt veniam. consectetur adipisicing elit. Quisquam a in mollitia expedita beatae eligendi ullam,
        deleniti iste nisi maiores ut, sed saepe magnam quos quaerat, labore nemo odio. Voluptates</p>
      
    </div>`;
    });
  // Get comments for each post
  fetch(
    `https://jsonplaceholder.typicode.com/posts/${id}/comments?postId=${id}`
  )
    .then(response => response.json())
    .then(json => {
      console.log(json);
      let comments = "";
      json.forEach(el => {
        comments += `<div class="comment"><div class="comment-content"><i class="fas fa-user user "></i><p>${el.body}</p><p>By:<span class='lorem'> ${el.name}</span></p><p class="date">October 9, 2019</p><i class="fas fa-thumbs-down icc"></i> <i class="fas fa-thumbs-up icc"></i>
				</div></span></div></div>`;
      });
      display3.innerHTML = `<h5>Comments</h5> <br> ${comments}`;
    });
}
