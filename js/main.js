// Save item to database
function saveDetail(key, data) {
  localStorage.setItem(key, JSON.stringify(data));
}

// Get item from database
function getItems(key) {
  localStorage.getItem(key);
  // console.log(item[0].jobNature);
}

// function getFromLocalStorage(key) {

//   let dataStore = localStorage.getItem(key);
//   if (dataStore) {
//     // check if data exists
//     return JSON.parse(dataStore);
//   } else {
//     return false;
//   }
// }

function generateRandId() {
  return new Date().getTime().toString() + Math.floor(Math.random() * 1000000);
}


function findById(id) {
  let jobsFromLocalStorage = getFromLocalStorage();
  console.log(jobsFromLocalStorage);
  if (Array.isArray(jobsFromLocalStorage)) {
    let targetJob = jobsFromLocalStorage.find(function (job) {
      return job.id === id;
    });

    return targetJob;
  }
}

function getFromLocalStorage() {
  let dataStore = localStorage.getItem("Details");
  if (dataStore) {
    // check if data exists
    return JSON.parse(dataStore);
  } else {
    return false;
  }
}