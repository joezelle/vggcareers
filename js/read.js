let jobListing = getFromLocalStorage();
// console.log(jobListing)

const addData = document.querySelector(".data");

function filteredSearch(data) {
  if (data !== null && data.length > 0) {
    let list = "";
    for (var i = data.length - 1; i >= 0; i--) {
      let currentJob = data[i];

      list += `<div class="single-post d-flex flex-row">
            <div class="details">
              <div class="title d-flex flex-row justify-content-between">
                <div class="titles">
                  <a href="singleJob.html">
                    <h4>${currentJob.jobTitle}</h4>
                  </a>
                  <h6>${currentJob.companyName}</h6>
                </div>
                <a href="view.html?id=${currentJob.id}" class="view">View details</a>

              </div>
              <p class="description">
              ${currentJob.jobSummary}
              </p>
              <h5>${currentJob.jobNature}</h5>
              <p class="address"> ${currentJob.location}</p>
              <p class="salary">${currentJob.salary}</p>
              <p class="published-on mr-5 d-inline">Published on: ${currentJob.publishedOn}</p>
              <p class="application-deadline d-inline">Application Deadline: ${currentJob.applicationDeadline} </p>
              
            </div>
          </div>`;
    }

    addData.innerHTML = list;
    console.log(list);
  } else {
    addData.innerHTML = `<div class="single-post d-flex flex-row"> <div class="titles">
          <h4 style="text-align:center">No Jobs Found</h4>
      </div></div>`;
  }
}

filteredSearch(jobListing);

function getNoOfJobs() {
  const jobList = getFromLocalStorage();
  if (Array.isArray(jobList)) {
    if (jobList) {
      let noOfJobs = jobList.length;
      return noOfJobs;
    } else {
      console.log("none");
      return 0;
    }
  } else {
    return 0;
  }
}

function renderNumberOfJobs() {
  // console.log(document.querySelector('#noOfJobs'));
  document.querySelector("#noOfJobs").innerHTML = getNoOfJobs();
}

renderNumberOfJobs();

function getJobCategories() {
  let availableCategories = [];

  let allCategories = getFromLocalStorage();
  if (allCategories) {
    let categories = allCategories;
    for (let i = 0; i < categories.length; i++) {
      let currentJob = categories[i];
      if (currentJob.hasOwnProperty("category")) {
        availableCategories.push(currentJob.category);
        // console.log(availableCategories)
      } else {
        return 0; // Falsy values => [ "" | 0 | "0" | false | undefined | null ]
      }
    }
    return availableCategories;
  } else {
    return undefined; // false
  }
}

function getJobLocation() {
  let availableLocation = [];

  let allLocation = getFromLocalStorage();
  if (allLocation) {
    let location = allLocation;
    for (let i = 0; i < location.length; i++) {
      let currentJob = location[i];
      if (currentJob.hasOwnProperty("location")) {
        availableLocation.push(currentJob.location);
        // console.log(availableCategories)
      } else {
        return 0; // Falsy values => [ "" | 0 | "0" | false | undefined | null ]
      }
    }
    return availableLocation;
  } else {
    return undefined; // false
  }
}

function renderJobCategories() {
  let selectCategory = document.querySelector("#catList");

  let categoryList = getJobCategories();
  console.log(categoryList);

  let bArray = categoryList.filter(cat => {
    if (cat === "Business") {
      return cat;
    }
  });
  let mediaArray = categoryList.filter(cat => {
    if (cat === "Media & News") {
      return cat;
    }
  });
  let medicalArray = categoryList.filter(cat => {
    if (cat === "Medical") {
      return cat;
    }
  });
  let legalArray = categoryList.filter(cat => {
    if (cat === "Legal") {
      return cat;
    }
  });
  let finArray = categoryList.filter(cat => {
    if (cat === "Finance") {
      return cat;
    }
  });
  let techArray = categoryList.filter(cat => {
    if (cat === "Technology") {
      return cat;
    }
  });
  let managementArray = categoryList.filter(cat => {
    if (cat === "Management") {
      return cat;
    }
  });
  // console.log(bArray.length)

  let list = "";
  list += `<li  class="justify-content-between d-flex">
    </span>Business<span>${bArray.length}</span></li>
    <li class="justify-content-between d-flex">
    <span>Technology</span><span>${techArray.length}</span></li>
    <li class="justify-content-between d-flex">
    <span>Finance</span><span>${finArray.length}</span></li><li class="justify-content-between d-flex">
    <span>Management</span><span>${managementArray.length}</span></li><li class="justify-content-between d-flex">
    <span>Media & News</span><span>${mediaArray.length}</span></li><li class="justify-content-between d-flex">
    <span>Medical</span><span>${medicalArray.length}</span></li><li class="justify-content-between d-flex">
    Legal<span></span><span>${legalArray.length}</span></li><li class="justify-content-between d-flex">
    <a `;
  selectCategory.innerHTML = list;
  console.log(selectCategory.innerHTML);
}

function renderJobLocation() {
  let selectCategory = document.querySelector("#locList");

  let locationList = getJobLocation();
  console.log(locationList);

  let lagosArray = locationList.filter(cat => {
    if (cat === "Lagos") {
      return cat;
    }
  });
  let abujaArray = locationList.filter(cat => {
    if (cat === "Abuja") {
      return cat;
    }
  });
  let portArray = locationList.filter(cat => {
    if (cat === "PortHarcourt") {
      return cat;
    }
  });
  let ibArray = locationList.filter(cat => {
    if (cat === "Ibadan") {
      return cat;
    }
  });
  let ilorinArray = locationList.filter(cat => {
    if (cat === "Ilorin") {
      return cat;
    }
  });
  let crossArray = locationList.filter(cat => {
    if (cat === "Crossriver") {
      return cat;
    }
  });
  let ondoArray = locationList.filter(cat => {
    if (cat === "Ondo") {
      return cat;
    }
  });
  let kanoArray = locationList.filter(cat => {
    if (cat === "Kano") {
      return cat;
    }
  });
  let josArray = locationList.filter(cat => {
    if (cat === "Jos") {
      return cat;
    }
  });
  // console.log(bArray.length)

  let list = "";
  list += `<li class="justify-content-between d-flex">
    <span>Lagos</span><span>${lagosArray.length}</span></li>
    <li class="justify-content-between d-flex">
    <span>Abuja</span><span>${abujaArray.length}</span></li>
    <li class="justify-content-between d-flex">
    <span>Jos</span><span>${josArray.length}</span></li><li class="justify-content-between d-flex">
    <span>Crossriver</span><span>${crossArray.length}</span></li><li class="justify-content-between d-flex">
    <span>PortHarcourt</span><span>${portArray.length}</span></li><li class="justify-content-between d-flex">
    <span>Ilorin</span><span>${ilorinArray.length}</span></li><li class="justify-content-between d-flex">
    Ibadan<span></span><span>${ibArray.length}</span></li><li class="justify-content-between d-flex">
    <span>Ondo</span><span>${ondoArray.length}</span></li><li class="justify-content-between d-flex">
    Kano<span></span><span>${kanoArray.length}</span></li>`;

  selectCategory.innerHTML = list;
}

renderJobCategories();
renderJobLocation();

function getFromLocalStorage() {
  let dataStore = localStorage.getItem("Details");
  if (dataStore) {
    // check if data exists
    return JSON.parse(dataStore);
  } else {
    return 0;
  }
}
