const search = document.forms["search"];

const jobslisted = document.querySelector("#jobslisted");

search.addEventListener("submit", searchJobs);

function searchJobs(e) {
  e.preventDefault();

  let searchJobNature = document.getElementById("jobNature").value.trim();
  let searchLocation = document.getElementById("searchLocation").value.trim();
  let searchJobTitle = document.getElementById("keywordSearch").value.trim();

  // console.log(jobNature, searchLocation, keywordSearch);

  const filteredJobs = [];

  if (
    (searchJobNature || searchJobNature != "") &&
    (searchLocation || searchLocation != "") &&
    (searchJobTitle || searchJobTitle != "")
  ) {
    const jobData = getFromLocalStorage();

    if (jobData.length > 0) {
      for (let i = 0; i < jobData.length; i++) {
        let currentJob = jobData[i];

        if (
          currentJob.location
            .toLowerCase()
            .includes(searchLocation.toLowerCase()) &&
          currentJob.jobTitle
            .toLowerCase()
            .includes(searchJobTitle.toLowerCase()) &&
          currentJob.jobNature
            .toLowerCase()
            .includes(searchJobNature.toLowerCase())
        ) {
          filteredJobs.push(currentJob);
        } else {
          continue;
        }
      }
      // console.log(filteredJobs);
      filteredSearch(filteredJobs);

      function getNoOfAvailableJobs() {
        const jobList = getFromLocalStorage();
        if (Array.isArray(jobList)) {
          if (jobList) {
            let noOfJobs = filteredJobs.length;
            return (document.querySelector("#noOfJobs").innerHTML = noOfJobs);
          } else {
            // console.log("none");
            return (document.querySelector("#noOfJobs").innerHTML = "0");
          }
        } else {
          return 0;
        }
      }
      getNoOfAvailableJobs();
    } else {
      console.log("There are no jobs available");
    }
  } else {
    console.log("invalid input");
  }
  searchJobNature = "";
  searchLocation = "";
  searchJobTitle = "";
}

function getNoOfJobs() {
  const jobList = getFromLocalStorage();
  if (Array.isArray(jobList)) {
    if (jobList) {
      let noOfJobs = jobList.length;
      return (document.querySelector("#noOfJobs").innerHTML = noOfJobs);
    } else {
      console.log("none");
      return (document.querySelector("#noOfJobs").innerHTML = "No");
    }
  } else {
    return 0;
  }
}

searchJobs();

function getFromLocalStorage() {
  let dataStore = localStorage.getItem("Details");
  if (dataStore) {
    // check if data exists
    return JSON.parse(dataStore);
  } else {
    return 0;
  }
}
