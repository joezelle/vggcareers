let jobListing = getFromLocalStorage();
// console.log(jobListing)

const addData = document.querySelector(".data");

function filteredSearch(data) {
  if (data !== null && data.length > 0) {
    let list = "";
    for (var i = data.length - 1; i >= 0; i--) {
      let currentJob = data[i];

      list += `<div class="single-post d-flex flex-row">
            <div class="details">
              <div class="title d-flex flex-row justify-content-between">
                <div class="titles">
                  <a href="singleJob.html">
                    <h4>${currentJob.jobTitle}</h4>
                  </a>
                  <h6>${currentJob.companyName}</h6>
                </div>

              </div>
              <p class="description">
              ${currentJob.jobSummary}
              </p>
              <h5>${currentJob.jobNature}</h5>
              <p class="address"> ${currentJob.location}</p>
              <p class="salary">${currentJob.salary}</p>
              <p class="published-on mr-5 d-inline">Published on: ${currentJob.publishedOn}</p>
              <p class="application-deadline d-inline">Application Deadline: ${currentJob.applicationDeadline} </p>
            </div>
          </div>`;
    }

    addData.innerHTML = list;
  } else {
    addData.innerHTML = `<div class="single-post d-flex flex-row"> <div class="titles">
          <h4>No Jobs Found</h4>
      </div></div>`;
  }
}

// Delete Job from DOM

filteredSearch(jobListing);

function getNoOfJobs() {
  const jobList = getFromLocalStorage();
  if (Array.isArray(jobList)) {
    if (jobList) {
      let noOfJobs = jobList.length;
      return noOfJobs;
    } else {
      console.log("none");
      return 0;
    }
  } else {
    return false;
  }
}

function renderNumberOfJobs() {
  // console.log(document.querySelector('#noOfJobs'));
  document.querySelector("#noOfJobs").innerHTML = getNoOfJobs();
}

renderNumberOfJobs();

function getFromLocalStorage() {
  let dataStore = localStorage.getItem("Details");
  if (dataStore) {
    // check if data exists
    return JSON.parse(dataStore);
  } else {
    return false;
  }
}
